/*eslint-env node */
"use strict";

// This module delegates to the npm module named "request" after first ensuring
//   that you are authenticated with PureCloud.

// npm
var _ = require("lodash");
var Promise = require("bluebird");
var request = require("request");
var requestAsync = Promise.promisify(request);

// for production purecloud, this should be "https://api.mypurecloud.com/api/v2"
var API_ROOT = "https://api.inindca.com/api/v2";

var makeRequest = function (method, route, token, data) {
    // if starts with http, don't add API_ROOT
    var isJson = false;
    var isFullUrl = route.search(/^http.*/) === 0;
    if (!isFullUrl) {
        route = API_ROOT + route;
        isJson = true;
    }
    var options = {
        method: method,
        url: route,
        headers: {
            Authorization: "bearer " + token
        }
    };
    if (data) {
        options.data=data;
    }
    if (isJson) {
        options.json=isJson;
        options['content-type'] = 'application/json';
    }
    return requestAsync(options);
};

// module
var mod = {
    // returns a promise
    get: function (route, token) {
        return makeRequest("GET", route, token);
    },
    put: function (route, token, data) {
        return makeRequest("PUT", route, token, data);
    },
    post: function (route, token, data) {
        return makeRequest("POST", route, token, data);
    },
    delete: function (route, token) {
        return makeRequest("DELETE", route, token);
    },
    del: function (route, token) {
        return makeRequest("DELETE", route, token);
    },
    patch: function (route, token, data) {
        return makeRequest("PATCH", route, token, data);
    },
    logout: function(token) {
        return makeRequest("GET", "https://login.inindca.com/logout", token);
    }
};

var exports = module.exports = mod;
