/*eslint-disable no-console */
(function (global, $, _, Promise, RestApi, URI) { // eslint-disable-line no-unused-vars
    "use strict";

    // This URI should be the URI to your website's back-end service.  In this
    //   case it is the service we have created locally.
    var serviceApi = new RestApi("https://127.0.0.1:8009");

    var AuthWidget = function (loginDomEl, logoutDomEl, redirectUrl) {
        var self = this;

        self.loginWidget = loginDomEl;
        self.$loginWidget = $(self.loginWidget);

        self.$loginWidget.on("click", function () {
            var uri = new URI(serviceApi.apiRoot+"/oauth/purecloud/login");
            uri.addQuery("redirect", redirectUrl);
            window.location.href = uri.toString();
        });

        self.logoutWidget = logoutDomEl;
        self.$logoutWidget = $(self.logoutWidget);

        self.$logoutWidget.on("click", function () {
            serviceApi.get("/oauth/purecloud/logout")
            .then(function (data) {
                console.log("Successfully Logged out of PureCloud.", data);
            })
            .catch(function (err) {
                console.log("Failed to Log out of PureCloud.", err);
            });
        });
    };

    global.AuthWidget = AuthWidget;

})(window, $, _, Promise, RestApi, URI); // eslint-disable-line no-undef
