/*eslint-disable no-console */
(function (global, $, _, Promise, RestApi) { // eslint-disable-line no-unused-vars
    "use strict";

    // This URI should be the URI to your website's back-end service.  In this
    //   case it is the service we have created locally.
    var serviceApi = new RestApi("https://127.0.0.1:8009");

    var MeWidget = function (meDomEl) {
        var self_ = this;

        var $meEl = $(meDomEl);

        $meEl.on("click", function() {
            serviceApi.get("purecloud/me")
            .then(function (data) {
                console.log("me data", data);
            })
            .catch(function (error) {
                console.log("me error", error);
            });
        });
    };

    global.MeWidget = MeWidget;

})(window, $, _, Promise, RestApi); // eslint-disable-line no-undef
