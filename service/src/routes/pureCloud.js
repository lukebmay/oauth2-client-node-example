/*eslint-env node */
/*eslint-disable no-console */
"use strict";

// npm
var _ = require("lodash");
var Promise = require("bluebird");
// local
var pureCloudUserApi = require("../services/pureCloud/pureCloudUserApi");

var mod = {};

mod.me = function (req, res, next) {
    return pureCloudUserApi.get("/users/me", req.bearerToken)
    .then(function (response) {
        if (typeof response.body === "string") {
            try {
                res.json(200, JSON.parse(response.body));
                return;
            }
            catch (e) {
                null;
            }
        }
        res.json(200, response.body);
    })
    .catch(function (err) {
        res.send(500, err);
    })
    .finally(function () {
        next();
    });
};

var exports = module.exports = mod;
