/*eslint-env node */
/*eslint-disable no-console */
"use strict";

// node
var fs = require("fs");

// var request = require("request");
// // npm request debugger:
// require("request-debug")(request);


// npm
var _ = require("lodash");
var restify = require("restify");

// local
var healthRoutes = require("./routes/health");
var oauthRoutes = require("./routes/oauth");
var pureCloudRoutes = require("./routes/pureCloud");

var server;
var IP = "127.0.0.1";
var PORT = "8009";

var startServer = function () {

    // These keys are for demo purposes and are NOT FOR PRODUCTION!!!!
    // DO NOT USE IN PRODUCTION CODE!!!
    var httpsKey = fs.readFileSync("./dist/service/https/server.key");
    var httpsCert = fs.readFileSync("./dist/service/https/server.crt");

    server = restify.createServer({
        key: httpsKey,
        certificate: httpsCert,
        rejectUnauthorized: false,
        name : "oauth2-client-node-example"
    });

    // restify tutorial:
    //     https://www.openshift.com/blogs/day-27-restify-build-correct-rest-web-services-in-nodejs

    // server.use(restify.CORS({
    //     origins: [
    //         'https://localhost:8080',
    //         'https://127.0.0.1:8080',
    //         'https://localhost:7200',
    //         'https://127.0.0.1:7200'
    //     ],
    //     headers: [
    //         "authorization",
    //         "inin-authorization",
    //         "x-inin-authorization",
    //         "withcredentials",
    //         "x-requested-with",
    //         "x-forwarded-for",
    //         "x-real-ip",
    //         "x-customheader",
    //         "user-agent",
    //         "keep-alive",
    //         "host",
    //         "accept",
    //         "connection",
    //         "upgrade",
    //         "content-type",
    //         "dnt",
    //         "if-modified-since",
    //         "cache-control"
    //     ],
    //     credentials: true
    // }));
    //
    // // server.use(restify.CORS());
    // server.use(restify.fullResponse()); // needed for CORS plugin to work
    server.use(restify.queryParser());
    server.use(restify.bodyParser({map: false}));
    server.use(function (req, res_, next) {
        var bearerToken = req.header("ININ-Authorization") || "";
        req.bearerToken = bearerToken.replace("bearer ", "");
        next();
    });

    //-------------------------------------------------------------------------
    // Options and CORS (what a nightmare)
    //-------------------------------------------------------------------------
    // Blanket CORS handling
    server.use(function (req_, res, next) {
        // var origins = [
        //     'https://localhost:8080',
        //     'https://127.0.0.1:8080',
        //     'https://localhost:7200',
        //     'https://127.0.0.1:7200'
        // ];
        var methods = [
            "GET",
            "POST",
            "PUT",
            "DELETE",
            "PATCH",
            "HEAD"
        ];
        var headers = [
            "ININ-Authorization",
            "inin-authorization",
            "authorization",
            "x-inin-authorization",
            "withcredentials",
            "x-requested-with",
            "x-forwarded-for",
            "x-real-ip",
            "x-customheader",
            "user-agent",
            "keep-alive",
            "host",
            "accept",
            "connection",
            "upgrade",
            "content-type",
            "dnt",
            "if-modified-since",
            "cache-control"
        ];
        res.setHeader("Access-Control-Allow-Origin", "*");
        res.setHeader("Access-Control-Allow-Methods", methods.join(", "));
        res.setHeader("Access-Control-Allow-Headers", headers.join(", "));
        next();
    });
    // Blanket OPTIONS method handling
    server.opts(/.*/, function (req_, res, next) {
        res.send(204);
        next();
    });


    //-------------------------------------------------------------------------
    // Default
    //-------------------------------------------------------------------------
    server.get("/", healthRoutes.healthCheck);

    //-------------------------------------------------------------------------
    // health-check / default (common / example existing customer site routes)
    //-------------------------------------------------------------------------
    server.get("/health", healthRoutes.healthCheck);
    server.get("/health/check", healthRoutes.healthCheck);
    server.get("/health-check", healthRoutes.healthCheck);
    server.get("/healthcheck", healthRoutes.healthCheck);

    //-------------------------------------------------------
    // User Login Redirect
    //-------------------------------------------------------
    server.get("/oauth/purecloud/login", oauthRoutes.purecloudLogin);
    server.get("/oauth/purecloud/logout", oauthRoutes.purecloudLogout);
    server.get("/oauth/purecloud/callback", oauthRoutes.purecloudCallback);

    //-------------------------------------------------------
    // PureCloudUserApi Proxies
    //-------------------------------------------------------
    server.get("purecloud/me", pureCloudRoutes.me);

    server.listen(PORT , IP);
};

//===================
// Process Messaging
//===================
process.on("message", function (msg) {
    if (msg === "start") {
        startServer();
    }
    if (msg === "stop") {
        server.close();
        process.exit(); // end program
    }
});

// start server if not running through child_process like in gulpfile.js
//   (e.g. node service/src/app.js)
if (!process.send) {
    startServer();
    console.log(server.name + " listening at " + server.url);
}
