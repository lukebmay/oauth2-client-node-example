/*eslint-env node */
/*eslint-disable no-console */
"use strict";

// npm
var _ = require("lodash");
var Promise = require("bluebird");
var simpleOauth2 = require("simple-oauth2");
var URI = require("urijs");

// local
var pureCloudUserApi = require("../services/pureCloud/pureCloudUserApi");

var CLIENT_ID = "8c01f9d6-8962-4f10-8fd5-024fcec84b59";
var CLIENT_SECRET = "zHxUL689GQrjA7tOUbmnqkB6nKmv2spTX8V5Xtszb6o";
var REDIRECT_URI = "https://127.0.0.1:8009/oauth/purecloud/callback";
var SITE_BASE = "https://login.inindca.com";
var TOKEN_PATH = "/oauth/token";
var AUTH_PATH = "/oauth/authorize";

var oauth2 = simpleOauth2({
    clientID: CLIENT_ID,
    clientSecret: CLIENT_SECRET,
    site: SITE_BASE,
    tokenPath: TOKEN_PATH,
    authorizationPath: AUTH_PATH,
    useBodyAuth: false
});

var mod = {};

mod.purecloudLogin = function (req, res, next) {
    var finalRedirectUri = req.query.redirect;
    var authorizationUri = oauth2.authCode.authorizeURL({
        // scope: 'notifications',
        state: finalRedirectUri,
        redirect_uri: REDIRECT_URI
    });
    res.redirect(authorizationUri, next);
};

mod.purecloudCallback = function (req, res, next) {
    var finalRedirectUri = req.query.state;
    var code = req.query.code;
    oauth2.authCode.getToken({
        code: code,
        redirect_uri: REDIRECT_URI
    })
    .then(function (result) {
        var token = oauth2.accessToken.create(result);
        var uri = new URI(finalRedirectUri);
        uri.addQuery("token", JSON.stringify(token));
        res.redirect(uri.toString(), next);
    })
    .catch(function (error) {
        console.log('Access Token Error', error);
        res.redirect("https://127.0.0.1:8008/autherror.html", next);
    });
};

mod.purecloudLogout = function (req_, res, next) {
    // purecloud logout redirects to a collaborate logout, from which you can't
    //   return automatically.  So instead of logging out, we are just going to
    //   delete the token, and so this endpoint will be a dummy.
    res.send(200);
    next();
    //res.redirect("https://login.inindca.com/logout", next);
};

var exports = module.exports = mod;
