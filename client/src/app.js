/*eslint-disable no-console */
(function (global, $, AuthWidget, MeWidget) { // eslint-disable-line no-unused-vars
    "use strict";

    console.log("Starting client...");


    // get dom element with which you want to make the widget
    var $loginEl = $("#login");
    var $logoutEl = $("#logout");
    var redirectUrl = "https://127.0.0.1:8008/";
    var authWidget = new AuthWidget($loginEl[0], $logoutEl[0], redirectUrl);
    global.authWidget = authWidget;

    // me widget
    var $meEl = $("#me");
    var meWidget = new MeWidget($meEl[0]);
    global.meWidget = meWidget;



})(window, $, AuthWidget, MeWidget); // eslint-disable-line no-undef
